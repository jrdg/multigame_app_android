package com.example.jordangauthier.gametest.vinnythefatty;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.example.jordangauthier.gametest.R;

import java.util.Random;

/**
 * Created by Jordan Gauthier on 9/2/2016.
 */
public class GameLoop extends Thread
{
    private boolean gameIsStart;
    private GameView gv;
    private Minuterie minuterie;
    private boolean restartgame;
    private boolean pause;
    private boolean makeTheSpriteMove;
    private boolean activityIsAlive;
    private boolean loose;
    private MediaPlayer gameover;

    private SharedPreferences preference;
    private SharedPreferences.Editor editor;

    private boolean newRecord;

    private Handler handler;


    public GameLoop(GameView gv)
    {
        gameover = MediaPlayer.create(gv.getCtx(), R.raw.gameovervinny);
        this.gameIsStart = false;
        this.gv = gv;
        this.minuterie = new Minuterie(0);
        newRecord = false;
        this.activityIsAlive = true;
        handler = gv.getHandler();
    }

    public void startGame()
    {
        this.gameIsStart = true;
        loose = false;

        if(this.gameIsStart)
        {
            this.start();
            this.spriteThread();
        }
    }

    @Override
    public void run()
    {
        do {
            pause = true;

            int timer = 3;
            int i = 0;

            makeTheSpriteMove = true;

            while (timer > -1)
            {
                gv.setTimer(timer);

                timer--;

                gv.postInvalidate();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            minuterie.startMinuterie("up");
            makeTheSpriteMove = false;

            while(gameIsStart)
            {
                Canvas canvas = null;
                try {
                    //on recupere le canvas
                    canvas = gv.getmHolder().lockCanvas();

                   //on syncronized pour pas quaun autre thread accede au surfaceholder
                    synchronized (gv.getmHolder())
                    {

                        //Log.d("debug" , "main game thread");

                        for(int h = 0 ; h <= gv.getDonutsList().size()-1 ; h++)
                        {
                            gv.getDonutsList().get(h).setX( gv.getDonutsList().get(h).getX() - 5);

                            if(Collision(gv.getChubby(),  gv.getDonutsList().get(h)))
                            {
                                gameIsStart = false;
                                loose = true;
                                gameover.start();
                            }

                            if( gv.getDonutsList().get(h).getX() < 0)
                                gv.getDonutsList().remove( gv.getDonutsList().get(h));
                        }

                        if(gv.getCloud().getRight() <= 0)
                        {
                            gv.getCloud().setLeft(gv.getWidth());
                            gv.getCloud().setRight(this.gv.getWidth()+gv.getNuage().getWidth());
                        }
                        else
                        {
                            gv.getCloud().setLeft(gv.getCloud().getLeft() - 5);
                            gv.getCloud().setRight(gv.getCloud().getRight() - 5);
                        }


                        if(i % 40 == 0)
                            generateCandy();

                        i++;

                        Log.d("debug" , "game thread");

                        gv.setMinuterie(minuterie.getTime());
                        gv.setStringMin(minuterie.getTimeString());

                        gv.postInvalidate();

                        Thread.sleep(1000/100);
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                finally {
                    //on unlock le canvas pour que le dessin saffiche
                    if(canvas != null)
                        gv.getmHolder().unlockCanvasAndPost(canvas);

                }
            }

            minuterie.stop();

            if(loose)
                finishPop();

            while(pause){}

        }while (restartgame);
    }

    public boolean Collision(Chubby chubby , Donut donut)
    {
        if (donut.getX() + donut.getRayon() >= chubby.getLeft() &&
                donut.getX() - donut.getRayon() <= chubby.getRight() &&
                donut.getY() + donut.getRayon() <= chubby.getBottom() &&
                donut.getY() - donut.getRayon() >= chubby.getTop()-10)
           return true;

        return false;
    }

    public void generateCandy()
    {
        Random r = new Random();
        int random = r.nextInt(2 - 1 + 1) + 1;

        float ratio = this.gv.getWidth() / this.gv.getHeight();

        if(random == 1)
        {
            this.gv.getDonutsList().add(new Donut(
                    this.gv.getWidth()+100,
                   // this.gv.getHeight()-(this.gv.getHeight()/4)-(this.gv.getHeight()/7),
                    this.gv.getHeight()-(this.gv.getHeight()/4)-this.gv.getChubbyBoy1().getHeight()/4,
                    ratio * 10
            ));
        }
        else if (random == 2)
        {
            this.gv.getDonutsList().add(new Donut(
                    this.gv.getWidth()+100,
                    this.gv.getHeight()-(this.gv.getHeight()/4)-20,
                    ratio * 10
            ));
        }
    }

    public void finishPop()
    {
        newRecord = false;
        preference = PreferenceManager.getDefaultSharedPreferences(gv.getCtx());
        editor = preference.edit();

        Log.d("debug",Float.toString(preference.getFloat("time",0)));

        float ancientRecord = preference.getFloat("time",0);
        float nouveauRecord = (float) minuterie.getTime();

        if(nouveauRecord > ancientRecord)
        {
            editor.putString("strTime",minuterie.getTimeString());
            editor.putFloat("time", (float) minuterie.getTime());
            editor.apply();
            newRecord = true;
        }



        if(!this.gv.getAc().isFinishing())
        {

            handler.post(new Runnable() {
                @Override
                public void run() {

                    AlertDialog.Builder builder = new AlertDialog.Builder(gv.getCtx());

                    if(newRecord)
                        builder.setTitle("CONGRATSTULATION NEW RECORD");
                    else
                        builder.setTitle("Game over");

                    builder.setMessage("You had been alive for : "+minuterie.getTimeString()+" minutes");
                    builder.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            setRestartgame(true);
                            gameIsStart = true;

                            gv.getDonutsList().clear();
                            gv.setMinuterie(0);
                            gv.setStringMin("0:00");
                            minuterie.resetTime();
                            gv.setTimer(3);
                            gv.getCloud().setLeft(gv.getWidth());
                            gv.getCloud().setRight(gv.getWidth()+gv.getNuage().getWidth());
                            loose = false;

                            pause = false;
                        }
                    });

                    builder.setNegativeButton("Back to menu", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            pause = false;
                            setRestartgame(false);
                            Log.d("debug",Boolean.toString(restartgame));
                            Intent intent = gv.getAc().getIntent();
                            gv.getAc().finish();
                        }
                    });

                    builder.setCancelable(false);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });

//            this.gv.getAc().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    AlertDialog.Builder builder = new AlertDialog.Builder(gv.getCtx());
//
//                    if(newRecord)
//                        builder.setTitle("CONGRATSTULATION NEW RECORD");
//                    else
//                        builder.setTitle("Game over");
//
//                    builder.setMessage("You had been alive for : "+minuterie.getTimeString()+" minutes");
//                    builder.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            setRestartgame(true);
//                            gameIsStart = true;
//
//                            gv.getDonutsList().clear();
//                            gv.setMinuterie(0);
//                            gv.setStringMin("0:00");
//                            minuterie.resetTime();
//                            gv.setTimer(3);
//                            gv.getCloud().setLeft(gv.getWidth());
//                            gv.getCloud().setRight(gv.getWidth()+gv.getNuage().getWidth());
//
//                            pause = false;
//                        }
//                    });
//
//                    builder.setNegativeButton("Back to menu", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            pause = false;
//                            setRestartgame(false);
//                            Log.d("debug",Boolean.toString(restartgame));
//                            Intent intent = gv.getAc().getIntent();
//                            gv.getAc().finish();
//                        }
//                    });
//
//                    builder.setCancelable(false);
//
//                    AlertDialog dialog = builder.create();
//                    dialog.show();
//                }
//            });
        }
    }

    public void spriteThread()
    {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(activityIsAlive)
                {
                    if(!gv.getChubby().isJumping() && !gv.getChubby().isSliding())
                    {
                        if(gv.getChubby().getRunningSpriteNumber() == 0)
                            gv.getChubby().setRunningSpriteNumber(1);
                        else if(gv.getChubby().getRunningSpriteNumber() == 1)
                            gv.getChubby().setRunningSpriteNumber(2);
                        else if(gv.getChubby().getRunningSpriteNumber() == 2)
                            gv.getChubby().setRunningSpriteNumber(3);
                        else if(gv.getChubby().getRunningSpriteNumber() == 3)
                            gv.getChubby().setRunningSpriteNumber(0);
                    }

                  //  Log.d("debug","before game starting");

                    if(makeTheSpriteMove)
                        gv.postInvalidate();

                    try {
                        Thread.sleep(55);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Log.d("debug" , "avant sprite thread");
                }
            }
        });

        t.start();
    }

    public boolean isRestartgame() {
        return restartgame;
    }

    public void setRestartgame(boolean restartgame) {
        this.restartgame = restartgame;
    }

    public void setActivityIsAlive(boolean activityIsAlive) {
        this.activityIsAlive = activityIsAlive;
    }

    public void stopAllThread()
    {
         this.gameIsStart = false;
         this.activityIsAlive = false;
         this.minuterie.stop();
    }

    public boolean isGameIsStart() {
        return gameIsStart;
    }
}
