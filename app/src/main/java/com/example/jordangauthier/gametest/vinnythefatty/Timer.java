package com.example.jordangauthier.gametest.vinnythefatty;

import android.util.Log;

/**
 * Created by Jordan Gauthier on 9/4/2016.
 */
public class Timer extends Thread {

    private int timer;

    public Timer(int timer)
    {
        this.timer = timer;
    }

    @Override
    public void run()
    {
        while(timer != 0)
        {
            timer--;
            Log.d("debug", Integer.toString(timer));

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public int getTimer() {
        return timer;
    }
}
