package com.example.jordangauthier.gametest.mariojetpack.Entity;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.jordangauthier.gametest.R;

import java.util.Random;

/**
 * Created by Jordan Gauthier on 9/11/2016.
 */
public class Cloud extends ItemObject {

    // constructeur pour initialiser le nuage
    public Cloud(Context context, int width, int height) {

        Random generateur = new Random();
        int choixNuage = generateur.nextInt(3);

        switch (choixNuage){
            case 0:
                setBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.cloudsmall));
                break;
            case 1:
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.cloudmedium);
                break;
            case 2:
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.cloudbig);
                break;
        }

        // réduire la taille des nuages pour les petits écrans
        scaleBitmap(width, height, 15, 12);

        maxX = width ;      // les nuages se déplacent uniquement horizontalement
        maxY = height / 3;  // les nuages sont en haut du ciel
        minX = 0;
        minY = 0;

        x = width;
        y = generateur.nextInt(maxY) - bitmap.getHeight();
        if(y < 0){
            y = minY;
        }
        speed = generateur.nextInt(3) + 2;    // generer entier aléatoire entre 2 et 4;
    }


    public void update(int playerSpeed){

        // déplacement à gauche
        // si mario accelere, les nuages se rapprochent plus vite
        x -= playerSpeed / 2;       // vitesse de notre mario
        x -= speed / 2;             // vitesse du nuage
    }
}

