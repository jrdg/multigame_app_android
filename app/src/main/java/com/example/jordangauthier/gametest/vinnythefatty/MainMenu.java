package com.example.jordangauthier.gametest.vinnythefatty;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.jordangauthier.gametest.R;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }
}
