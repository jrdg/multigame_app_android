package com.example.jordangauthier.gametest.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jordangauthier.gametest.R;
import com.example.jordangauthier.gametest.mariojetpack.View.Main2Activity;
import com.example.jordangauthier.gametest.vinnythefatty.MainActivity;
import com.example.jordangauthier.gametest.vinnythefatty.MainMenu;
import com.example.jordangauthier.gametest.vinnythefatty.MenuActivity;

import java.util.ArrayList;
import java.util.List;

public class menu extends Activity {

    private ListView lv;
    private MenuAdapter adapter;
    private ArrayList<Game> gameList;
    private SharedPreferences preference;
    private SharedPreferences.Editor editor;
    private Context ctx;
    private ImageView footer;
    private ImageView header;
    private RelativeLayout rl;
    private int width;
    private int height;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        rl = new RelativeLayout(this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        height = dm.heightPixels;
        width = dm.widthPixels;

        this.ctx = this;

        lv = new ListView(this);

        preference = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preference.edit();

        String vinnyTheFattyHIghScore = preference.getString("strTime","No record");
        long marioTetPackHighScore = preference.getLong("bestScore" , 0);

        gameList = new ArrayList<>();
        gameList.add(new Game("Vinny the fatty" , vinnyTheFattyHIghScore , R.drawable.jumping4));
        gameList.add(new Game("Mario jetpack" , Long.toString(marioTetPackHighScore) , R.drawable.marioicon));
        gameList.add(new Game("Space invader" , "none" , R.drawable.running2));
        gameList.add(new Game("Metroid" , "none" , R.drawable.running2));
        gameList.add(new Game("Pokemon" , "none" , R.drawable.running2));
        gameList.add(new Game("Lava lash" , "none" , R.drawable.running2));
        gameList.add(new Game("Minecraft" , "none" , R.drawable.running2));
        gameList.add(new Game("Counter-strike" , "none" , R.drawable.running2));
        gameList.add(new Game("World of warcraft" , "none" , R.drawable.running2));
        gameList.add(new Game("Hockey" , "none" , R.drawable.running2));
        gameList.add(new Game("Football" , "none" , R.drawable.running2));

        adapter = new MenuAdapter(this,0,gameList,width,height);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Game game = (Game) adapterView.getItemAtPosition(i);

                Intent intent = null;

                if(game != null)
                {
                    if(game.getName().equals("Vinny the fatty"))
                        intent = new Intent(ctx, MenuActivity.class);
                    else if(game.getName().equals("Mario jetpack"))
                        intent = new Intent(ctx, Main2Activity.class);

                    if(game.getHighscore().equals("none"))
                    {
                        Toast toast = Toast.makeText(ctx, "This game is not available yet", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }

                if(intent != null)
                    startActivity(intent);
            }
        });


        RelativeLayout.LayoutParams paramss = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramss.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        header = new ImageView(this);
        header.setAdjustViewBounds(true);
        header.setMaxWidth(width);
        header.setMaxHeight(height);
        header.setImageResource(R.drawable.logotp);
        header.setBackgroundColor(Color.parseColor("#302f2f"));
        header.setLayoutParams(paramss);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        footer = new ImageView(this);
        footer.setAdjustViewBounds(true);
        footer.setMaxWidth(width);
        footer.setMaxHeight(height);
        footer.setImageResource(R.drawable.footer);
        footer.setBackgroundColor(Color.parseColor("#302f2f"));
        footer.setLayoutParams(params);


        header.setId(+1);
        footer.setId(+2);

        RelativeLayout.LayoutParams paramsss = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsss.addRule(RelativeLayout.BELOW,header.getId());
        paramsss.addRule(RelativeLayout.ABOVE,footer.getId());
        lv.setLayoutParams(paramsss);

        rl.addView(header);
        rl.addView(lv);
        rl.addView(footer);

        setContentView(rl);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updateList();
    }

    public void updateList()
    {
        String vinnyTheFattyHIghScore = preference.getString("strTime","No record");
        long marioTetPackHighScore = preference.getLong("bestScore", 0000000);

        gameList.clear();
        gameList.add(new Game("Vinny the fatty" , vinnyTheFattyHIghScore , R.drawable.jumping4));
        gameList.add(new Game("Mario jetpack" , Long.toString(marioTetPackHighScore) , R.drawable.marioicon));
        gameList.add(new Game("Space invader" , "none" , R.drawable.spaceinvadericon));
        gameList.add(new Game("Metroid" , "none" , R.drawable.metroidicon));
        gameList.add(new Game("Pokemon" , "none" , R.drawable.pokemonicon));
        gameList.add(new Game("Lava lash" , "none" , R.drawable.lavalashicon));
        gameList.add(new Game("Minecraft" , "none" , R.drawable.minecrafticon));
        gameList.add(new Game("Counter-strike" , "none" , R.drawable.countericon));
        gameList.add(new Game("World of warcraft" , "none" , R.drawable.wowicon));
        gameList.add(new Game("Hockey" , "none" , R.drawable.hockeyicon));
        gameList.add(new Game("Football" , "none" , R.drawable.footballicon));

        adapter.notifyDataSetChanged();
    }
}
