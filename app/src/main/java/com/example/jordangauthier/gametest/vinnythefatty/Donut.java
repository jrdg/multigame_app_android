package com.example.jordangauthier.gametest.vinnythefatty;

/**
 * Created by Jordan Gauthier on 9/3/2016.
 */
public class Donut
{
    private float x;
    private float y;
    private float rayon;

    private boolean isAlive;

    public Donut()
    {
        this.isAlive = true;
    }

    public Donut(float x , float y , float rayon)
    {
        this.isAlive = true;
        this.x = x;
        this.y = y;
        this.rayon = rayon;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getRayon() {
        return rayon;
    }

    public void setRayon(float rayon) {
        this.rayon = rayon;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }
}
