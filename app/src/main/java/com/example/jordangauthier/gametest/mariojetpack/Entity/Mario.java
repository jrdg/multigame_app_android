package com.example.jordangauthier.gametest.mariojetpack.Entity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.jordangauthier.gametest.R;
import com.example.jordangauthier.gametest.mariojetpack.Entity.ItemObject;

/**
 * Created by Jordan Gauthier on 9/11/2016.
 */
public class Mario extends ItemObject {
    Context context;
    private boolean acceleration;       // acceleration de mario
    private final int GRAVITE = -15;    // gravité qui ralenti mario

    // limiter la vitesse de mario
    private final int MIN_SPEED = 1;    // vitesse minimale de mario
    private final int MAX_SPEED = 25;   // vitesse maximale de mario

    private int vie = 5;                // le nombre de vie de mario

    // rectangle de detection de collision
    private Rect collision;

    // constructeur pour initialiser mario
    public Mario(Context context, int width, int height) {

        this.context = context;
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.mario);

        x = 50;
        y = 50;
        speed = 1;
        acceleration = false;
        maxY = height - bitmap.getHeight();
        minY = 0;

        // Initialiser le rectangle de collision
        collision = new Rect(width, height, bitmap.getWidth(), bitmap.getHeight());
    }

    public void update() {

        if (acceleration) {
            // accelerer
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.mariofast);
            speed += 2;
        } else {
            // ralentir
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.mario);
            speed -= 5;
        }
        // limiter la vitesse max de mario
        if (speed > MAX_SPEED) {
            speed = MAX_SPEED;
        }
        // limiter la vitesse min de mario
        if (speed < MIN_SPEED) {
            speed = MIN_SPEED;
        }
        // déplacer mario du haut en bas
        y -= speed + GRAVITE;
        // garder mario à l'interieur de l'écran
        if (y < minY) {
            y = minY;
        }
        if (y > maxY) {
            y = maxY;
        }

        // Rafréchir la position du rectangle collision
        collision.left = x;
        collision.top = y;
        collision.right = x + bitmap.getWidth();
        collision.bottom = y + bitmap.getHeight();
    }

    public void accelererOn(){
        acceleration=true;
    }

    public void accelererOff(){
        acceleration=false;
    }

    public void diminuerVie(){
        vie --;
    }

    public void augmenterVie(){
        vie ++;
    }

    public Rect getCollision() {
        return collision;
    }

    public int getVie() {
        return vie;
    }
}
