package com.example.jordangauthier.gametest.vinnythefatty;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.example.jordangauthier.gametest.R;
import com.example.jordangauthier.gametest.vinnythefatty.GamePanel;

public class MainActivity extends Activity {

    private GamePanel gp;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int height = dm.heightPixels;
        int width = dm.widthPixels;

        gp = new GamePanel(this,width,height,this);

        this.setContentView(gp);

       // mp = MediaPlayer.create(getApplicationContext(), R.raw.pacman);
       // mp.setLooping(true);
       // mp.start();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if(gp.getGameView().getLoop() != null)
        {
            Log.d("debug" , "boom");
            gp.getGameView().getLoop().stopAllThread();
        }

    }
}
