package com.example.jordangauthier.gametest.vinnythefatty;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.jordangauthier.gametest.R;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by Jordan Gauthier on 9/2/2016.
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback
{
    private SurfaceHolder mHolder = null;
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private GameLoop loop;

    //largeur et hauteur du canvas du jeu
    private int width;
    private int height;

    //instance de chubbyBoy
    private Chubby chubby;

    //instance de Nudge
    private Nuage cloud;

    private Context ctx;

    private float ratio;

    //instance dun tableau de donut
    private ArrayList<Donut> donutsList;

    //image de chubby boy
    Bitmap chubbyBoy1;
    Bitmap chubbyBoy2;
    Bitmap chubbyBoy3;
    Bitmap chubbyBoy4;
    Bitmap chubbyBoyJumping1;
    Bitmap chubbyBoySliding1;
    Bitmap nuage;

    private int timer;
    private double minuterie;
    private String stringMin;

    private Activity ac;

    public GameView(Context context , int width , int height , Activity ac)
    {
        super(context);
        this.ctx = context;
        this.ac = ac;
        this.init();
        this.setBackgroundColor(Color.parseColor("#00BFFF"));
        this.width = width;
        this.height = height;

        ratio = width/height;

        //on va chercher les image des sprite pour le chubbyboy
        this.chubbyBoy1 = BitmapFactory.decodeResource(getResources(), R.drawable.running1);
        this.chubbyBoy2 = BitmapFactory.decodeResource(getResources(),R.drawable.running2);
        this.chubbyBoy3 = BitmapFactory.decodeResource(getResources(),R.drawable.running3);
        this.chubbyBoy4 = BitmapFactory.decodeResource(getResources(),R.drawable.running4);
        this.chubbyBoyJumping1 = BitmapFactory.decodeResource(getResources(),R.drawable.jumping4);
        this.chubbyBoySliding1 = BitmapFactory.decodeResource(getResources(),R.drawable.sliding5);
        this.nuage = BitmapFactory.decodeResource(getResources(),R.drawable.nuage);

        cloud = new Nuage();
        cloud.setLeft(this.width);
        cloud.setTop(10);
        cloud.setRight(this.width+this.nuage.getWidth());
        cloud.setBottom(10+this.nuage.getHeight());

        //on instancie lentite pour chubbyboy
        this.chubby = new Chubby();
        this.chubby.setDiplayWidth(this.width);
        this.chubby.setDisplayHeight(this.height);
        this.chubby.setSpriteToRender(this.chubbyBoy1);

        this.donutsList = new ArrayList<>();

        this.timer = 3;
        this.minuterie = 0;
        this.stringMin = "0:00";
    }

    public GameView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    //quand la surface es cree
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder)
    {
        this.loop = new GameLoop(this);
        this.loop.startGame();
    }

    //quand lutilisateur change le telephone dorientation par exemple
    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2)
    {

    }

    //quand la surface es detruite on detruit les thread normalement on arrete tous.
    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder)
    {

    }

    @Override
    public void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        if(this.timer > -1)
        {
            paint.setColor(Color.BLACK);
            paint.setTextSize(100);
            canvas.drawText(Integer.toString(this.timer),this.width/2,this.height/2,paint);

            if(timer == 0)
                timer = -1;
        }

        canvas.drawBitmap(this.nuage,null,new RectF(cloud.getLeft(),cloud.getTop(),cloud.getRight(),cloud.getBottom()),null);

        paint.setColor(Color.BLACK);
        paint.setTextSize(20);
        canvas.drawText(this.stringMin,this.width/2,100,paint);

        paint.setColor(Color.YELLOW);
        canvas.drawCircle(0,0,150,paint);
        paint.setColor(Color.parseColor("#4DBD33"));
        canvas.drawRect(0,this.height-(this.height/4),this.width,this.height,paint);


        if(this.chubby.isJumping())
        {
            this.chubby.setSpriteToRender(this.chubbyBoyJumping1);
            this.chubby.setLeft(this.width-(this.width*2/3)-this.chubby.getWidth() / (int)ratio/4);
            this.chubby.setRight(this.width-(this.width*2/3));
        }
        else if(this.chubby.isSliding())
        {
            this.chubby.setSpriteToRender(this.chubbyBoySliding1);
            this.chubby.setLeft(this.width-(this.width*2/3)-this.chubby.getWidth() / (int)ratio/4);
            this.chubby.setTop(this.height-(this.height/4)-this.chubby.getHeight() / (int)ratio/4);
            this.chubby.setRight(this.width-(this.width*2/3));
            this.chubby.setBottom(this.height-(this.height/4));
        }
        else
        {
            if(this.chubby.getRunningSpriteNumber() == 0)
                this.chubby.setSpriteToRender(this.chubbyBoy2);
            else if(this.chubby.getRunningSpriteNumber() == 1)
                this.chubby.setSpriteToRender(this.chubbyBoy3);
            else if(this.chubby.getRunningSpriteNumber() == 2)
                this.chubby.setSpriteToRender(this.chubbyBoy4);
            else if(this.chubby.getRunningSpriteNumber() == 3)
                this.chubby.setSpriteToRender(this.chubbyBoy1);

            this.chubby.setLeft(this.width-(this.width*2/3)-this.chubby.getWidth()/ (int)ratio/4);
            this.chubby.setTop(this.height-(this.height/4)-this.chubby.getHeight()/ (int)ratio/4);
            this.chubby.setRight(this.width-(this.width*2/3));
            this.chubby.setBottom(this.height-(this.height/4));
        }


        //on dessine chubbyboy
        canvas.drawBitmap(this.chubby.getSpriteToRender(), null, new RectF(chubby.getLeft(),chubby.getTop(),chubby.getRight(),chubby.getBottom()), null);

        paint.setColor(Color.MAGENTA);

        for(int i = 0 ; i <= this.donutsList.size()-1 ; i++)
            canvas.drawCircle(this.donutsList.get(i).getX(),this.donutsList.get(i).getY(),this.donutsList.get(i).getRayon(),paint);
    }

    public void init()
    {
        this.mHolder = getHolder();
        this.mHolder.addCallback(this);
    }

    public SurfaceHolder getmHolder() {
        return mHolder;
    }

    public void setmHolder(SurfaceHolder mHolder) {
        this.mHolder = mHolder;
    }

    public Chubby getChubby() {
        return chubby;
    }

    public void setChubby(Chubby chubby) {
        this.chubby = chubby;
    }

    public ArrayList<Donut> getDonutsList()
    {
        return this.donutsList;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public void setStringMin(String stringMin) {
        this.stringMin = stringMin;
    }

    public void setMinuterie(double minuterie) {
        this.minuterie = minuterie;
    }

    public Context getCtx() {
        return ctx;
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }

    public Activity getAc() {
        return ac;
    }

    public Bitmap getChubbyBoy1() {
        return chubbyBoy1;
    }

    public GameLoop getLoop() {
        return loop;
    }

    public Nuage getCloud() {
        return cloud;
    }

    public Bitmap getNuage() {
        return nuage;
    }
}
