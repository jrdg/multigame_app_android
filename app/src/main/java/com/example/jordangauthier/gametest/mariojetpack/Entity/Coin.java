package com.example.jordangauthier.gametest.mariojetpack.Entity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.jordangauthier.gametest.R;

import java.util.Random;

/**
 * Created by Jordan Gauthier on 9/11/2016.
 */
public class Coin extends ItemObject {

    // rectangle de detection de collision
    private Rect collision;

    // constructeur pour initialiser le coin
    public Coin(Context context, int width, int height) {

        Random generateur = new Random();

        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.coin);


        // réduire la taille des nuages pour les petits écrans
        scaleBitmap(width, height, 20, 15);

        maxX = width ;
        maxY = height - bitmap.getHeight();
        minX = 0;
        minY = 0;

        x = width;
        y = generateur.nextInt(maxY) - bitmap.getHeight();
        if(y < 0){
            y = minY;
        }
        speed = generateur.nextInt(6)+10;    // generer entier aléatoire entre 10 et 15;

        // Initialiser le rectangle de collision
        collision = new Rect(width, height, bitmap.getWidth(), bitmap.getHeight());
    }

    public void update(int playerSpeed){

        // déplacement à gauche
        // si mario accelere, les ennemis se rapprochent plus vite
        x -= playerSpeed / 2;       // vitesse de notre mario
        x -= speed / 2;             // vitesse de coin


        // Rafréchir la position du rectangle collision
        collision.left = x;
        collision.top = y;
        collision.right = x + bitmap.getWidth();
        collision.bottom = y + bitmap.getHeight();
    }

    public Rect getCollision() {
        return collision;
    }

}
