package com.example.jordangauthier.gametest.vinnythefatty;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.jordangauthier.gametest.R;

/**
 * Created by Jordan Gauthier on 9/4/2016.
 */
public class MenuPanel extends LinearLayout
{
    private Button play;
    private Button rules;
    private Button option;
    private Button leave;

    public MenuPanel(Context context) {
        super(context);
        this.init(context);
    }

    public MenuPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init(Context context)
    {
        this.setBackgroundResource(R.drawable.menupage);
        this.setOrientation(VERTICAL);
        this.setGravity(Gravity.CENTER);
        this.setPadding(150,0,150,0);

        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0,5,0,5);

        this.play = new Button(context);
        this.play.setText("Play");
        this.play.setBackgroundColor(Color.parseColor("#fac2f0"));
        this.play.setLayoutParams(params);

        this.rules = new Button(context);
        this.rules.setText("Rules");
        this.rules.setBackgroundColor(Color.parseColor("#fac2f0"));
        this.rules.setLayoutParams(params);

        this.option = new Button(context);
        this.option.setText("Best score");
        this.option.setBackgroundColor(Color.parseColor("#fac2f0"));
        this.option.setLayoutParams(params);

        this.leave = new Button(context);
        this.leave.setText("Quit game");
        this.leave.setBackgroundColor(Color.parseColor("#fac2f0"));
        this.leave.setLayoutParams(params);

        this.addView(play);
        this.addView(this.rules);
        this.addView(this.option);
        this.addView(leave);

        this.initListener(context);
    }

    public void initListener(final Context context)
    {
        Button[] barray = {this.play,this.rules,this.option,this.leave};

        for(int i = 0 ; i <= barray.length-1 ; i++)
            barray[i].setId(i+1);

        for(final Button button : barray)
        {
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = null;

                    if(button.getId() == 1f)
                        intent = new Intent(context , MainActivity.class );
                    else if(button.getId() == 2f)
                        intent = new Intent(context , RulesPage.class);
                    else if(button.getId() == 3f)
                        intent = new Intent(context , ScorePage.class);
                    else if(button.getId() == 4f)
                        ((Activity)context).finish();

                    if(intent != null)
                        context.startActivity(intent);
                }
            });
        }
    }
}
