package com.example.jordangauthier.gametest.vinnythefatty;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;

/**
 * Created by Jordan Gauthier on 9/2/2016.
 */
public class Chubby {
    //position sur le canvas de chubby
    private float left;
    private float top;
    private float right;
    private float bottom;

    private int diplayWidth;
    private int displayHeight;

    //dimension de chubby
    private int width;
    private int height;

    //action que chubby peu faire
    private boolean isRunning;
    private boolean isSliding;
    private boolean isJumping;

    //les boolean pour savoir si ON PEU faire telle action
    private boolean canJump;
    private boolean canSlide;

    //quel sprite afficher
    private int runningSpriteNumber = 0;

    //image a afficher
    private Bitmap spriteToRender;

    public Chubby() {
        this.canJump = true;
        this.canSlide = true;
        this.isRunning = true;
        this.isSliding = false;
        this.isJumping = false;
    }

    //method
    public float getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public float getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public float getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public float getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Bitmap getSpriteToRender() {
        return spriteToRender;
    }

    public void setSpriteToRender(Bitmap spriteToRender) {
        this.width = spriteToRender.getWidth();
        this.height = spriteToRender.getHeight();
        this.spriteToRender = spriteToRender;
    }

    public int getRunningSpriteNumber() {
        return runningSpriteNumber;
    }

    public void setRunningSpriteNumber(int runningSpriteNumber) {
        this.runningSpriteNumber = runningSpriteNumber;
    }

    public boolean isCanJump() {
        return canJump;
    }

    public void setCanJump(boolean canJump) {
        this.canJump = canJump;
    }

    public boolean isCanSlide() {
        return canSlide;
    }

    public void setCanSlide(boolean canSlide) {
        this.canSlide = canSlide;
    }

    public boolean isJumping() {
        return isJumping;
    }

    public void setJumping(boolean jumping) {
        isJumping = jumping;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public boolean isSliding() {
        return isSliding;
    }

    public void setSliding(boolean sliding) {
        isSliding = sliding;
    }

    public int getDiplayWidth() {
        return diplayWidth;
    }

    public void setDiplayWidth(int diplayWidth) {
        this.diplayWidth = diplayWidth;
    }

    public int getDisplayHeight() {
        return displayHeight;
    }

    public void setDisplayHeight(int displayHeight) {
        this.displayHeight = displayHeight;
    }

    //methode d'action
    public void jumpAction(final float topBeforeJump)
    {
        this.isJumping = true;
        this.canJump = false;
        this.canSlide = false;

        Thread jump = new Thread(new Runnable() {
            @Override
            public void run() {

                boolean up = true;

                float velocityY = 15f;
                float gravity = 2.5f;

                while(isJumping)
                {
                    top -= velocityY;
                    bottom -= velocityY;
                    velocityY -= gravity;

                    if(top >= topBeforeJump)
                    {
                        isJumping = false;
                        canJump = true;
                        canSlide = true;
                        top = topBeforeJump;
                    }

                    try {
                        Thread.sleep(33);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        jump.start();
    }
}
