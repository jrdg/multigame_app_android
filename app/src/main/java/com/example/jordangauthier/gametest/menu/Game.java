package com.example.jordangauthier.gametest.menu;

/**
 * Created by Jordan Gauthier on 9/10/2016.
 */
public class Game
{
    private String name;
    private String highscore;
    private int image;

    public Game(String name , String highscore , int image)
    {
        this.name = name;
        this.highscore = highscore;
        this.image = image;
    }

    public String getHighscore() {
        return highscore;
    }

    public void setHighscore(String highscore) {
        this.highscore = highscore;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
