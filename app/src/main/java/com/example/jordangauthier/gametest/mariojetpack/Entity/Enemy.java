package com.example.jordangauthier.gametest.mariojetpack.Entity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.jordangauthier.gametest.R;

import java.util.Random;

/**
 * Created by Jordan Gauthier on 9/11/2016.
 */
public class Enemy extends ItemObject {

    // rectangle de detection de collision
    private Rect collision;
    private int zigzagEnemy;

    boolean zigzagDirection = true;
    private int count = 0;

    // constructeur pour initialiser l'ennemi
    public Enemy(Context context, int width, int height) {

        Random generateur = new Random();

        zigzagEnemy = generateur.nextInt(2);    // generer entier aléatoire 0 ou 1;

        int choixEnnemi = generateur.nextInt(3);

        switch (choixEnnemi){
            case 0:
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.flyguy);
                break;
            case 1:
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.flyturtle);
                break;
            case 2:
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.missile1);
                break;
        }

        // réduire la taille des nuages pour les petits écrans
        scaleBitmap(width, height, 15, 12);

        maxX = width ;
        maxY = height - bitmap.getHeight();
        minX = 0;
        minY = 0;

        x = width;
        y = generateur.nextInt(maxY) - bitmap.getHeight();
        if(y < 0){
            y = minY;
        }
        speed = generateur.nextInt(6)+10;    // generer entier aléatoire entre 10 et 15;

        // Initialiser le rectangle de collision
        collision = new Rect(width, height, bitmap.getWidth(), bitmap.getHeight());
    }

    public void update(int playerSpeed){

        // déplacement à gauche
        // si mario accelere, les ennemis se rapprochent plus vite
        x -= playerSpeed / 2;       // vitesse de notre mario
        x -= speed / 2;             // vitesse de l'ennemi

        Random generateur = new Random();
        int countMax = generateur.nextInt(20) +30;    // generer entier aléatoire entre 0 et 49;

        if(count == countMax) {
            zigzagDirection = !zigzagDirection;
            count = 0;
        }
        if(zigzagEnemy == 1 && count < countMax) {
            if(zigzagDirection) {
                y += 2;
                if(y > maxY)
                    y = maxY;
                count++;
            }
            else{
                y -= 2;
                if(y < minY)
                    y = minY;
                count++;
            }
        }
        // Rafréchir la position du rectangle collision
        collision.left = x;
        collision.top = y;
        collision.right = x + bitmap.getWidth();
        collision.bottom = y + bitmap.getHeight();
    }

    public Rect getCollision() {
        return collision;
    }

}