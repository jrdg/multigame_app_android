package com.example.jordangauthier.gametest.mariojetpack.Entity;

import android.graphics.Bitmap;

/**
 * Created by Jordan Gauthier on 9/11/2016.
 */
public class ItemObject {
    protected Bitmap bitmap;  // graphique de l'object
    protected int x, y;       // coordonnées de l'object
    protected int speed = 1;  // vitesse de l'object

    // Empécher l'object de sortir de l'écran
    protected int maxX;       // position max horizontale de l'object
    protected int minX;       // position min horizontale de l'object
    protected int maxY;       // position max verticale de l'object
    protected int minY;       // position min verticale de l'object


    // réduire la taille des nuages pour les petits écrans
    public void scaleBitmap(int width, int height, int ratioWidth, int ratioHight){
        bitmap = Bitmap.createScaledBitmap(bitmap,
                width / ratioWidth,
                height / ratioHight,
                false);
    }

    // getter et setter
    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }

    public int getMinX() {
        return minX;
    }

    public void setMinX(int minX) {
        this.minX = minX;
    }

    public int getMinY() {
        return minY;
    }

    public void setMinY(int minY) {
        this.minY = minY;
    }
}
