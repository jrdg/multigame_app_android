package com.example.jordangauthier.gametest.vinnythefatty;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.jordangauthier.gametest.R;

import java.io.IOException;

/**
 * Created by Jordan Gauthier on 9/2/2016.
 */
public class GamePanel extends RelativeLayout
{
    //le linear layout qui va contenir les bouton
    private LinearLayout ll;
    private int width;
    private int height;

    //les bouton
    private Button jumpButton;
    private Button slideButton;
    private int ratio;

    private MediaPlayer jump;
    private MediaPlayer slide;

    //la gameview
    private GameView gameView;

    private Activity ac;

    public GamePanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    public GamePanel(Context context , int width , int height , Activity ac) {
        super(context);
        this.ac = ac;
        this.height = height;
        this.width = width;
        ratio = this.width / this.height;
        this.init(context);
    }

    public GamePanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public void init(final Context context)
    {
        //les parametre du linear layout que va contenir les boutons
        RelativeLayout.LayoutParams paramsLL = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLL.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        //on instancie le linearlayout des bouton
        this.ll = new LinearLayout(context);
        this.ll.setLayoutParams(paramsLL);


        //on instancie les boutons
        this.jumpButton = new Button(context);
        this.slideButton = new Button(context);
        jumpButton.setPadding(ratio*50,ratio * 50,ratio * 50,ratio * 50);
        this.slideButton.setPadding(ratio*50,ratio * 50,ratio * 50,ratio * 50);

        //on set les texts sur les bouton de control
        this.jumpButton.setText("^");
        this.slideButton.setText("S");

        LinearLayout llShoot = new LinearLayout(context);
        llShoot.setGravity(Gravity.RIGHT);

        //on instancie la game view
        this.gameView = new GameView(context,this.width,this.height,this.ac);

        this.addView(this.gameView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        this.addView(this.ll);
        this.ll.addView(this.jumpButton,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.ll.addView(llShoot, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        llShoot.addView(this.slideButton , new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

         jump = MediaPlayer.create(ac.getApplicationContext(), R.raw.jump11);
         slide = MediaPlayer.create(ac.getApplicationContext(), R.raw.slide);

        this.jumpButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == motionEvent.ACTION_DOWN)
                {
                    if(gameView.getChubby().isCanJump())
                    {
                        jump.start();
                        gameView.getChubby().jumpAction(gameView.getChubby().getTop());
                    }
                }

                return true;
            }
        });

//        this.jumpButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(gameView.getChubby().isCanJump())
//                {
//                   // MediaPlayer mp = MediaPlayer.create(ac.getApplicationContext(), R.raw.jump11);
//                   // mp.start();
//                    gameView.getChubby().jumpAction(gameView.getChubby().getTop());
//                }
//            }
//        });


        this.slideButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(gameView.getChubby().isCanSlide())
                {
                    if(motionEvent.getAction() == motionEvent.ACTION_DOWN)
                    {
                        slide.start();
                        gameView.getChubby().setSliding(true);
                    }
                    else if(motionEvent.getAction() == motionEvent.ACTION_UP)
                        gameView.getChubby().setSliding(false);
                }
                return true;
            }
        });
    }


    public GameView getGameView() {
        return gameView;
    }
}
