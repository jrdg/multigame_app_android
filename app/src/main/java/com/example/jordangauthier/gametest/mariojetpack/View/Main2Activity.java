package com.example.jordangauthier.gametest.mariojetpack.View;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jordangauthier.gametest.R;

public class Main2Activity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //chargement du UI (User Interface) layout provenant de activity_main.xml
        setContentView(R.layout.activity_main2);

        // Preparer le chargement du meilleur temps
        SharedPreferences prefs;
        SharedPreferences.Editor editor;
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Creation du bouton avec sa reférence dans le UI layout
        final Button buttonPlay = (Button) findViewById(R.id.buttonPlay);

        // reference au TextView dans notre layout
        final TextView higherScore =
                (TextView)findViewById(R.id.textHighScore);

        // Listener du bouton
        buttonPlay.setOnClickListener(this);

        // chargement du meilleur score, par defaut 0000000
        long bestScore = prefs.getLong("bestScore", 0000000);
        // afficher le meilleur temps dans TextView
        higherScore.setTextColor(Color.rgb(0, 0, 0));
        higherScore.setText("High score:" + bestScore);
    }

    @Override
    public void onClick(View view) {
        // Pour le bouton start.
        Intent i = new Intent(this, GameActivity.class);
        // démarrage de la classe GameActivity à traver Intent
        startActivity(i);
        // Terminer l'activité
        finish();
    }

}
