package com.example.jordangauthier.gametest.mariojetpack.Manager;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.jordangauthier.gametest.R;
import com.example.jordangauthier.gametest.mariojetpack.Entity.Cloud;
import com.example.jordangauthier.gametest.mariojetpack.Entity.Coin;
import com.example.jordangauthier.gametest.mariojetpack.Entity.Enemy;
import com.example.jordangauthier.gametest.mariojetpack.Entity.Etoile;
import com.example.jordangauthier.gametest.mariojetpack.Entity.LifeBonus;
import com.example.jordangauthier.gametest.mariojetpack.Entity.Mario;
import com.example.jordangauthier.gametest.mariojetpack.Entity.TimeBonus;

import java.util.ArrayList;

/**
 * Created by Jordan Gauthier on 9/11/2016.
 */
public class GameView extends SurfaceView implements Runnable{

    volatile boolean enJeu; // volatile pour avoir accés dans le thread et en dehors
    Thread gameThread = null;

    private boolean finJeu;   // fin du jeu

    // declaration des objects graphique du jeu
    private Mario mario;
    public Enemy missile1;
    public Enemy flyturtle;
    public Enemy flyGuy;
    public Cloud cloud1;
    public Cloud cloud2;
    public Cloud cloud3;
    public Cloud cloud4;
    public Cloud cloud5;
    public Coin coin1;
    public Coin coin2;
    public Coin coin3;
    public Coin coin4;
    public Coin coin5;
    public TimeBonus timeBonus;
    public LifeBonus lifeBonus;

    // dimension de l'écran
    private int width;
    private int height;

    // le son
    private SoundPool sp;
    // reference de son pour le jouer aprés
    int startId = -1;
    int bumpId = -1;
    int loseId = -1;
    int winId = -1;
    int musicId = -1;
    int coinId = -1;
    int lifeId = -1;
    int timeId = -1;
    int streamMusic = -1; // pour arreter la musique lorsque la partie est terminé

    // Pour déssiner
    private Context context;
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder ourHolder; // pour vérouiller canvas lorsqu'on le manipule et de le dévérouiller lorsqu'on déssine

    // Arraylist des étoiles aléatoires
    public ArrayList<Etoile> etoileList = new ArrayList<Etoile>();

    // les stats
    private long timeTaken;
    private long timeStarted;
    private long tempsJeu;
    private long tempsRestant;
    private long bestScore;
    private long score = 0;

    // Pour le chargement du meilleur temps
    private SharedPreferences prefs;
    // Pour sauvegarder le meilleur temps
    private SharedPreferences.Editor editor;

    // constructeur
    public GameView(Context context, int width, int height) {
        super(context);
        this.context  = context;
        this.width = width;
        this.height = height;

        // soundPool est déprécié, mais ne nécessite pas API 21 comme son remplacant SoundPool.Builder
        // SoundPool est meilleur que mediaplayer pour les sons de courtes durées
        // SoundPool (int maxStreams, int streamType, int srcQuality)
        // 10 est le nombre de son max qu'on peut jouer en même temps
        // AudioManager.STREAM_MUSIC est le type de son FX
        // 0 est la qualité
        sp = new SoundPool(10, AudioManager.STREAM_MUSIC,0);

        // reference de son pour le jouer aprés
        startId = sp.load(context, R.raw.start, 0);
        winId = sp.load(context, R.raw.win, 0);
        bumpId = sp.load(context, R.raw.bump, 0);
        coinId = sp.load(context, R.raw.coin, 0);
        lifeId = sp.load(context, R.raw.life, 0);
        timeId = sp.load(context, R.raw.time, 0);
        loseId = sp.load(context, R.raw.lose, 0);
        musicId = sp.load(context, R.raw.music, 0);

        // initialisation des objects de déssin
        ourHolder = getHolder();
        paint = new Paint();

        // chargement du meilleur temps à traver le fichier HiScores
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Initialisation de editor
        editor = prefs.edit();
        // lecture du meilleur score dont la clef est bestScore
        // par defaut highscore = 0000000
        bestScore = prefs.getLong("bestScore", 0000000);
        editor.apply();

        startGame();
    }

    private void startGame(){

        // la partie du jeu n'est pas terminé
        finJeu = false;
        // Initialiser les objects graphiques
        mario = new Mario(context, width, height);
        missile1 = new Enemy(context, width, height);
        flyturtle = new Enemy(context, width, height);
        flyGuy = new Enemy(context, width, height);
        cloud1 = new Cloud(context, width, height);
        cloud2 = new Cloud(context, width, height);
        cloud3 = new Cloud(context, width, height);
        cloud4 = new Cloud(context, width, height);
        cloud5 = new Cloud(context, width, height);
        coin1 = new Coin(context, width, height);
        coin2 = new Coin(context, width, height);
        coin3 = new Coin(context, width, height);
        coin4 = new Coin(context, width, height);
        coin5 = new Coin(context, width, height);
        lifeBonus = new LifeBonus(context, width, height);
        timeBonus = new TimeBonus(context, width, height);

        // initialisation des étoiles
        int nbEtoiles = 30;
        for (int i = 0; i < nbEtoiles; i++) {
            // position de l'étoile aléatoire avec chaque instanciation
            Etoile spec = new Etoile(width, height);
            etoileList.add(spec);
        }

        // Reset le timer
        timeTaken = 0;
        // temps de début
        timeStarted = System.currentTimeMillis();
        tempsJeu =  100000L; // commence la partie avec 100 secondes
        tempsRestant = tempsJeu;

        // play(int soundID, float leftVolume, float rightVolume, int priority, int loop, float rate)
        SystemClock.sleep(500);
        sp.play(startId, 1, 1, 0, 0, 1);
        SystemClock.sleep(500);
        streamMusic = sp.play(musicId, 0.5F, 0.5F, 0, 999, 1);
    }

    @Override
    public void run() {
        while(enJeu){
            update();
            draw();
            control();
        }
    }

    private void update() {

        // tester la collision en premier
        // Si il ya collision, on instancie de nouveau
        // ainsi il va apparaitre à droite de l'écran


        // collision monstres
        boolean hitDetected = false;
        if(Rect.intersects(mario.getCollision(), missile1.getCollision())){
            hitDetected = true;
            mario.diminuerVie();
            missile1 = new Enemy(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), flyturtle.getCollision())){
            hitDetected = true;
            mario.diminuerVie();
            flyturtle = new Enemy(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), flyGuy.getCollision())){
            hitDetected = true;
            mario.diminuerVie();
            flyGuy = new Enemy(context, width, height);
        }

        // collision coins
        if(Rect.intersects(mario.getCollision(), coin1.getCollision())){
            score++;
            sp.play(coinId, 1, 1, 0, 0, 1);
            coin1 = new Coin(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), coin2.getCollision())){
            score++;
            sp.play(coinId, 1, 1, 0, 0, 1);
            coin2 = new Coin(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), coin3.getCollision())){
            score++;
            sp.play(coinId, 1, 1, 0, 0, 1);
            coin3 = new Coin(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), coin4.getCollision())){
            score++;
            sp.play(coinId, 1, 1, 0, 0, 1);
            coin4 = new Coin(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), coin5.getCollision())){
            score++;
            sp.play(coinId, 1, 1, 0, 0, 1);
            coin5 = new Coin(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), coin5.getCollision())){
            score++;
            sp.play(coinId, 1, 1, 0, 0, 1);
            coin5 = new Coin(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), lifeBonus.getCollision())){
            mario.augmenterVie();
            sp.play(lifeId, 1, 1, 0, 0, 1);
            lifeBonus = new LifeBonus(context, width, height);
        }
        if(Rect.intersects(mario.getCollision(), timeBonus.getCollision())){
            tempsJeu += 10000L; // bonus de 10 secondes
            sp.play(timeId, 1, 1, 0, 0, 1);
            timeBonus = new TimeBonus(context, width, height);
        }

        // si collision, la vie diminue
        // lorsque c'est nulle, la partie est terminé
        if(hitDetected) {
            sp.play(bumpId, 1, 1, 0, 0, 1);
            if (mario.getVie() == 0) {
                sp.play(loseId, 1, 1, 0, 0, 1);
                finJeu = true;
            }
        }

        if (tempsRestant <= 0) {
//            sp.play(loseId, 1, 1, 0, 0, 1);
            finJeu = true;
        }

        // si les objects graphiques sortent de l'écran à gauche, on les instancie de nouveau
        if(missile1.getX() < -missile1.getBitmap().getWidth())
            missile1 = new Enemy(context, width, height);
        if(flyturtle.getX() < -flyturtle.getBitmap().getWidth())
            flyturtle = new Enemy(context, width, height);
        if(flyGuy.getX() < -flyGuy.getBitmap().getWidth())
            flyGuy = new Enemy(context, width, height);
        if(cloud1.getX() < -cloud1.getBitmap().getWidth())
            cloud1 = new Cloud(context, width, height);
        if(cloud2.getX() < -cloud2.getBitmap().getWidth())
            cloud2 = new Cloud(context, width, height);
        if(cloud3.getX() < -cloud3.getBitmap().getWidth())
            cloud3 = new Cloud(context, width, height);
        if(cloud4.getX() < -cloud4.getBitmap().getWidth())
            cloud4 = new Cloud(context, width, height);
        if(cloud5.getX() < -cloud5.getBitmap().getWidth())
            cloud5 = new Cloud(context, width, height);
        if(coin1.getX() < -coin1.getBitmap().getWidth())
            coin1 = new Coin(context, width, height);
        if(coin2.getX() < -coin2.getBitmap().getWidth())
            coin2 = new Coin(context, width, height);
        if(coin3.getX() < -coin3.getBitmap().getWidth())
            coin3 = new Coin(context, width, height);
        if(coin4.getX() < -coin4.getBitmap().getWidth())
            coin4 = new Coin(context, width, height);
        if(coin5.getX() < -coin5.getBitmap().getWidth())
            coin5 = new Coin(context, width, height);
        if(lifeBonus.getX() < -lifeBonus.getBitmap().getWidth())
            lifeBonus = new LifeBonus(context, width, height);
        if(timeBonus.getX() < -timeBonus.getBitmap().getWidth())
            timeBonus = new TimeBonus(context, width, height);

        // rafréchir mario
        mario.update();

        // rafréchir les ennemis
        missile1.update(mario.getSpeed());
        flyturtle.update(mario.getSpeed());
        flyGuy.update(mario.getSpeed());

        // rafréchir les nuages
        cloud1.update(mario.getSpeed());
        cloud2.update(mario.getSpeed());
        cloud3.update(mario.getSpeed());
        cloud4.update(mario.getSpeed());
        cloud5.update(mario.getSpeed());

        // rafréchir les coins
        coin1.update(mario.getSpeed());
        coin2.update(mario.getSpeed());
        coin3.update(mario.getSpeed());
        coin4.update(mario.getSpeed());
        coin5.update(mario.getSpeed());

        // rafréchir le lifeBonus
        lifeBonus.update(mario.getSpeed());
        // rafréchir le timeBonus
        timeBonus.update(mario.getSpeed());

        // rafréchir les étoiles
        for (Etoile sd : etoileList) {
            sd.update(mario.getSpeed());
        }

        // si la partie n'est pas finie
        if(!finJeu) {
            // durée de la partie du jeu
            timeTaken = System.currentTimeMillis() - timeStarted;
            tempsRestant =  tempsJeu - timeTaken; // commence la partie avec 100 secondes
        }

        // vérifier si meilleur temps
        if(score > bestScore) {
            // sauvegarde du meilleur temps
            editor.putLong("bestScore", score);
            editor.commit();
            bestScore = score;
        }
    }

    private void draw() {
        // vérifier si la classe SurfaceHolder est valide
        if (ourHolder.getSurface().isValid()) {
            //Vérouiller canvas
            canvas = ourHolder.lockCanvas();
            // ecran de couleur blue ciel
            canvas.drawColor(Color.argb(255, 106, 90, 205));

            // étoile en couleur blanche
            paint.setColor(Color.argb(255, 255, 255, 255));
            //déssiner les étoiles contenus dans notre arrayList
            for (Etoile sd : etoileList) {
                canvas.drawCircle(sd.getX() + sd.getRayon()/2, sd.getY() + sd.getRayon()/2, sd.getRayon(), paint);
            }

            // déssiner les objects graphiques
            canvas.drawBitmap(mario.getBitmap(), mario.getX(), mario.getY(), paint);
            canvas.drawBitmap(missile1.getBitmap(), missile1.getX(), missile1.getY(), paint);
            canvas.drawBitmap(flyturtle.getBitmap(), flyturtle.getX(), flyturtle.getY(), paint);
            canvas.drawBitmap(flyGuy.getBitmap(), flyGuy.getX(), flyGuy.getY(), paint);
            canvas.drawBitmap(cloud1.getBitmap(), cloud1.getX(), cloud1.getY(), paint);
            canvas.drawBitmap(cloud2.getBitmap(), cloud2.getX(), cloud2.getY(), paint);
            canvas.drawBitmap(cloud3.getBitmap(), cloud3.getX(), cloud3.getY(), paint);
            canvas.drawBitmap(cloud4.getBitmap(), cloud4.getX(), cloud4.getY(), paint);
            canvas.drawBitmap(cloud5.getBitmap(), cloud5.getX(), cloud5.getY(), paint);
            canvas.drawBitmap(coin1.getBitmap(), coin1.getX(), coin1.getY(), paint);
            canvas.drawBitmap(coin2.getBitmap(), coin2.getX(), coin2.getY(), paint);
            canvas.drawBitmap(coin3.getBitmap(), coin3.getX(), coin3.getY(), paint);
            canvas.drawBitmap(coin4.getBitmap(), coin4.getX(), coin4.getY(), paint);
            canvas.drawBitmap(coin5.getBitmap(), coin5.getX(), coin5.getY(), paint);
            canvas.drawBitmap(lifeBonus.getBitmap(), lifeBonus.getX(), lifeBonus.getY(), paint);
            canvas.drawBitmap(timeBonus.getBitmap(), timeBonus.getX(), timeBonus.getY(), paint);

            // déssiner les stats
            if(!finJeu) {   // partie non terminée
                paint.setTextAlign(Paint.Align.LEFT);
                paint.setColor(Color.argb(255, 255, 255, 255));
                paint.setTextSize(25);
                canvas.drawText("Score:" + score, 10, 20, paint);
                canvas.drawText("Time Left:" + tempsRestant/1000 + "s", width / 2, 20, paint);
                canvas.drawText("Vie:" + mario.getVie(), width / 3, height - 20, paint);
                // la vitesse 600 initiale est arbitraire
                canvas.drawText("Speed:" + mario.getSpeed() * 20 + " KM/h", (width / 3) * 2, height - 20, paint);
            }
            else{   // partie terminée
                // arreter la musique
                sp.stop(streamMusic);

                // jeu en pause avec les stats de la partie terminée
                paint.setTextSize(80);
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("Fin partie", width/2, 140, paint);
                paint.setTextSize(25);
                canvas.drawText("High Score:" + bestScore, width/2, 200, paint);
                canvas.drawText("Time Left:" + tempsRestant/1000 + "s", width / 2, 240, paint);
                paint.setTextSize(40);
                canvas.drawText("play", 100, height - 60, paint);
                canvas.drawText("Iam done", width - 130, height - 60, paint);

                mario.setX(-width);
            }
            // dévérouiller canvas et déssiner la scéne
            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void control() {
        try {
            gameThread.sleep(1000/80); // pour obtenir un FPS de 80
        } catch (InterruptedException e) {
        }
    }



    // arréter le thread si le jeu est intérompu ou le joueur quitte le jeu
    public void pause() {
        enJeu = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
        }
    }
    // lancer un nouveau thread
    public void resume() {
        enJeu = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    // detection des touches
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            // joueur touche l'écran avec son doigt
            case MotionEvent.ACTION_DOWN:
                mario.accelererOn();

                if(motionEvent.getX() < 150 && motionEvent.getY() > height  - 150) {
                    // si la pause de fin partie, rejouer
                    if (finJeu) {
                        score = 0;
                        startGame();
                    }
                }

                if(motionEvent.getX() > width - 150 && motionEvent.getY() > height  - 150) {
                    // si la pause de fin partie, quitter
                    if (finJeu) {
                        ((Activity) context).finish();
                    }
                }

                break;


            // joeur enléve son doigt de l'écran
            case MotionEvent.ACTION_UP:
                mario.accelererOff();
                break;

        }
        return true;
    }

    public SoundPool getSp() {
        return sp;
    }
}

