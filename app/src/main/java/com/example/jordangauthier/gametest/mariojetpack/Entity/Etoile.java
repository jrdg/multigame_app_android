package com.example.jordangauthier.gametest.mariojetpack.Entity;

import java.util.Random;

/**
 * Created by Jordan Gauthier on 9/11/2016.
 */
public class Etoile {

    private int x, y;       // coordonnées du centre de l'étoile
    private int rayon;      // rayon de l'étoile
    private int speed;      // vitesse de l'étoile

    // Empécher les étoiles de sortir de l'écran
    private int maxX;       // position max horizontale de l'étoile
    private int maxY;       // position max verticale de l'étoile

    // constructeur pour initialiser les étoiles
    public Etoile(int width, int height) {

        maxX = width ;      // étoile se déplacent uniquement horizontalement
        maxY = height;

        Random generateur = new Random();
        // position de l'étoile aléatoire dans l'écran
        x = generateur.nextInt(maxX);
        y = generateur.nextInt(maxY);
        speed = generateur.nextInt(10);    // generer entier aléatoire entre 0 et 9;
        rayon = generateur.nextInt(3);     // generer entier aléatoire entre 0 et 2;
    }

    public void update(int playerSpeed){

        // déplacement à gauche
        // si mario accelere, les étoiles se rapprochent plus vite
        x -= playerSpeed;       // vitesse de mario
        x -= speed;             // vitesse de l'étoile
        // résurrection l'étoile lorsqu'elle sort de l'écran à gauche
        if(x < 0){
            Random generateur = new Random();
            speed = generateur.nextInt(10);
            rayon = generateur.nextInt(3);
            x = maxX;
            y = generateur.nextInt(maxY);
        }
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public int getRayon() {
        return rayon;
    }
}
