package com.example.jordangauthier.gametest.mariojetpack.View;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.example.jordangauthier.gametest.mariojetpack.Manager.GameView;

public class GameActivity extends Activity {

    private GameView gameView;

    // le bouton start nous raméne ici
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        gameView = new GameView(this, width, height);
        setContentView(gameView);

    }

    // mettre le thread en pause
    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
        gameView.getSp().release(); //jai rajouter sa detruire tes son quan don quitte le jeu
    }

    // Resume le thread
    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }
}