package com.example.jordangauthier.gametest.vinnythefatty;

import android.content.Context;
import android.util.Log;

/**
 * Created by Jordan Gauthier on 9/4/2016.
 */

public class Minuterie
{
    private double time;
    private int theInt;
    private boolean isStart;

    public Minuterie(double time)
    {
        this.isStart = false;
        this.time = time;
        theInt = 0;
    }

    private void upTime()
    {
        this.time += 0.01;

        if(this.time + 0.40 >= theInt)
        {
            this.time += 0.40;
            this.theInt++;
        }
    }

    private void downTime()
    {
        this.time -= 0.01;

        if(this.time < theInt)
        {
            if(this.time + 0.40 >= theInt)
            {
                theInt--;
                this.time = theInt + 0.59;
            }
        }
    }

    public void resetTime()
    {
        this.time = 0;
        this.theInt = 1;
    }

    public double getTime()
    {
        return this.time;
    }

    public String getTimeString()
    {
        time = (double)Math.round(time * 100d) / 100d;
        String time = Double.toString(this.time);

        if(theInt < 10)
        {
            if(time.length() < 4)
                time += "0";
        }
        else if(theInt >= 11 && theInt < 100)
        {
            if(time.length() < 5)
                time += "0";
        }
        else if(theInt >= 101)
        {
            if(time.length() < 6)
                time += "0";
        }

        return time.replace('.', ':');
    }

    public void stop()
    {
        this.isStart = false;
    }

    public void startMinuterie(final String type)
    {
        this.isStart = true;

        if(type.equals("up"))
            this.theInt = (int)Math.ceil(time)+1;
        else
            this.theInt = (int)Math.ceil(time);

        Thread t = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while(isStart)
                {
                   // Log.d("debug" , "minuterie thread");

                    if(type.equals("up"))
                        upTime();
                    else
                        downTime();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        t.start();
    }

    public boolean isStarted()
    {
        if(this.isStart)
            return true;
        else
            return false;
    }
}