package com.example.jordangauthier.gametest.vinnythefatty;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.jordangauthier.gametest.R;

public class ScorePage extends Activity {

    private TextView score;
    private SharedPreferences preference;
    private SharedPreferences.Editor editor;
    private Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_score_page);

        preference = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preference.edit();

        String record = preference.getString("strTime","No record");

        score = (TextView) findViewById(R.id.score);
        score.setTextColor(Color.WHITE);
        score.setTextSize(25);

        if(!record.equals("No record"))
            score.setText(record+" Minutes");
        else
            score.setText(record);

        this.back = (Button) findViewById(R.id.backMenu);

        this.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                finish();
            }
        });

    }
}
