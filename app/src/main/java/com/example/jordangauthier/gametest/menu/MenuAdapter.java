package com.example.jordangauthier.gametest.menu;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Jordan Gauthier on 9/10/2016.
 */
public class MenuAdapter extends ArrayAdapter<Game> {

    private Context ctx;
    private int width;
    private int height;

    public MenuAdapter(Context context, int resource) {
        super(context, resource);
        this.ctx = context;
    }

    public MenuAdapter(Context context, int resource, List<Game> objects , int width , int height) {
        super(context, resource, objects);
        this.ctx = context;
        this.width = width;
        this.height = height;
    }

    public MenuAdapter(Context context, int resource, Game[] objects) {
        super(context, resource, objects);
        this.ctx = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Game game = getItem(position);

        LinearLayout global = new LinearLayout(ctx);
        global.setPadding(20,20,20,20);

        LinearLayout verticalLL = new LinearLayout(ctx);
        verticalLL.setPadding(20,0,0,0);

        ImageView img = new ImageView(ctx);

        TextView gameName = new TextView(ctx);
        gameName.setTypeface(Typeface.DEFAULT_BOLD);
        TextView highscore = new TextView(ctx);
        gameName.setText(game.getName());
        highscore.setText("High score : "+game.getHighscore());

        if(game.getHighscore().equals("none"))
            highscore.setText("GAME NOT AVAILABLE");

        global.setOrientation(LinearLayout.HORIZONTAL);
        verticalLL.setOrientation(LinearLayout.VERTICAL);

        img.setImageResource(game.getImage());
        img.setAdjustViewBounds(true);
        img.setMaxWidth(width/10);
        img.setMaxHeight(height/10);

        global.addView(img, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        global.addView(verticalLL, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        verticalLL.addView(gameName,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        verticalLL.addView(highscore, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        convertView = global;

        return convertView;
    }
}
