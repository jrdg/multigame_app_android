package com.example.jordangauthier.gametest.vinnythefatty;

/**
 * Created by Jordan Gauthier on 9/9/2016.
 */
public class Nuage
{
    private float left;
    private float top;
    private float right;
    private float bottom;

    public float getBottom() {
        return bottom;
    }

    public void setBottom(float bottom) {
        this.bottom = bottom;
    }

    public float getLeft() {
        return left;
    }

    public void setLeft(float left) {
        this.left = left;
    }

    public float getRight() {
        return right;
    }

    public void setRight(float right) {
        this.right = right;
    }

    public float getTop() {
        return top;
    }

    public void setTop(float top) {
        this.top = top;
    }
}
